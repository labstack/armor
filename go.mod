module gitlab.com/labstack/armor

require (
	github.com/Knetic/govaluate v3.0.0+incompatible
	github.com/armon/go-metrics v0.0.0-20180917152333-f0300d1749da // indirect
	github.com/asdine/storm v2.1.2+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/ghodss/yaml v1.0.0
	github.com/hashicorp/go-immutable-radix v1.0.0 // indirect
	github.com/hashicorp/go-msgpack v0.0.0-20150518234257-fa3f63826f7c // indirect
	github.com/hashicorp/go-multierror v1.0.0 // indirect
	github.com/hashicorp/go-sockaddr v0.0.0-20180320115054-6d291a969b86 // indirect
	github.com/hashicorp/logutils v1.0.0
	github.com/hashicorp/memberlist v0.1.0 // indirect
	github.com/hashicorp/serf v0.8.1
	github.com/jmoiron/sqlx v1.2.0
	gitlab.com/labstack/echo v3.3.7+incompatible
	gitlab.com/labstack/gommon v0.2.7
	gitlab.com/labstack/tunnel-client v0.2.12
	github.com/lib/pq v1.0.0
	github.com/miekg/dns v1.0.15 // indirect
	github.com/mitchellh/go-homedir v1.0.0
	github.com/mitchellh/mapstructure v1.1.2
	github.com/sean-/seed v0.0.0-20170313163322-e2103e2c3529 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/valyala/fasttemplate v0.0.0-20170224212429-dcecefd839c4
	go.etcd.io/bbolt v1.3.0 // indirect
	golang.org/x/crypto v0.0.0-20181106171534-e4dc69e5b2fd
)
